<?php 

class ACFFields{
	public static function _init(){
		add_action( 'acf/input/admin_footer', 'ACFFields::admin_footer' );
		add_action( 'acf/init', 'ACFFields::add_acf_fields' );
		add_action( 'acf/init', 'ACFFields::add_options_page' );
		add_action( 'acf/init', 'ACFFields::update_gkey' );
	}
	public static function update_gkey(){
		acf_update_setting('google_api_key', get_field('gmaps-api-key', 'option'));
	}
	public static function admin_footer() {	
		?>
		<script type="text/javascript">
			(function($) {
				
				$('#acf-field_71y21gfad12 .acf-gallery-add').text('Add to Slider');
				
			})(jQuery);	
		</script>
		<?php
	}
	public static function add_options_page(){
		if( function_exists( 'acf_add_options_page' ) ) {
				
			acf_add_options_page(array(
				'page_title' 	=> 'General Settings',
				'menu_title' 	=> 'General Settings',
				'menu_slug' 	=> 'general-settings',
			));
			
		}
	}
	public static function add_acf_fields(){
		acf_add_local_field_group(array(
			'key' => 'group_1',
			'title' => ' ',
			'fields' => array (
				array(
					'key' => 'field_nh1212adf',
					'label' => '<h1 style="text-align: center;">Page Settings</h1>',
					'type' => 'message',
				),
				array(
					'key' => 'field_bfguawef',
					'label' => 'Hero Image',
					'name' => 'hero-image',
					'type' => 'image',
					'return_format' => 'url',
				),
				array(
					'key' => 'field_71y21gfad12',
					'label' => '<h1 style="text-align: center;">Build Your Page Here</h1>',
					'name' => 'page-builder',
					'type' => 'flexible_content',
					'button_label' => 'Add to Page',
					'layouts' => array(
						array(
							'key' => 'field_7216gafaef',
							'label' => 'Map Module',
							'name' => 'map_module',
							'sub_fields' => array(
								array(
									'key' => 'field_bdap2721ada98',
									'label' => 'Map',
									'name' => 'map',
									'type' => 'google_map',
									'center_lat' => '37.786748',	
									'center_lng' => '-122.398325',
								),
							),
						),
						array(
							'key' => 'field_n1812bbaddzdfeeeaf',
							'label' => 'Content Module',
							'name' => 'content_module',
							'sub_fields' => array(
								array(
									'key' => 'field_zpsadfh23',
									'type' => 'wysiwyg',
									'name' => 'content',
									'label' => 'Content',
								),
								array(
									'key' => 'field_opafhen12',
									'type' => 'color_picker',
									'name' => 'bg_color',
									'label' => 'Background Color',
								),
							),
						),
						array(
							'key' => 'field_n1812baf',
							'label' => 'Image Module',
							'name' => 'image_module',
							'sub_fields' => array(
								array(
									'key' => 'field_nm1y12gae',
									'label' => 'Image',
									'name' => 'image',
									'type' => 'image',
									'return_format' => 'url',
								),
								array(
									'key' => 'field_n1h8h2had',
									'label' => 'Caption?',
									'name' => 'caption_toggle',
									'type' => 'true_false',
									'wrapper' => array(
										'width' => '50',
									),
									'instructions' => 'Tick this box if you want there to be a caption displayed in the middle of this module',
								),
								array(
									'key' => 'field_pkdfyehgaf',
									'label' => 'Caption Text',
									'name' => 'caption',
									'type' => 'text',
									'wrapper' => array(
										'width' => '50',
									),
									'conditional_logic' => array(
										array(
											array(
												'field' => 'field_n1h8h2had',
												'operator' => '==',
												'value' => 1,
											),
										),
									),
								),
								array(
									'key' => 'field_sweifhdk2',
									'label' => 'Link?',
									'instructions' => 'Tick this box if you want the caption to become a link',
									'name' => 'link_toggle',
									'type' => 'true_false',
									'wrapper' => array(
										'width' => '50',
									),
								),
								array(
									'key' => 'field_sweizzzzzzzfhdk2',
									'label' => 'Open in New Tab?',
									'instructions' => 'Tick this box if you want the link to open in a new tab',
									'name' => 'link_newtab',
									'type' => 'true_false',	
									'wrapper' => array(
										'width' => '50',
									),
									'conditional_logic' => array(
										array(
											array(
												'field' => 'field_sweifhdk2',
												'operator' => '==',
												'value' => 1,
											),
										),
									),
								),
								array(
									'key' => 'field_121anafheha',
									'label' => 'Link Type',
									'type' => 'select',
									'name' => 'link_type',
									'wrapper' => array(
										'width' => '50',
									),
									'choices' => array(
										'page' => 'Page',
										'url' => 'URL',
										'email' => 'Email',
									),
									'conditional_logic' => array(
										array(
											array(
												'field' => 'field_sweifhdk2',
												'operator' => '==',
												'value' => 1,
											),
										),
									),
								),
								array(
									'key' => 'field_721g21ba',
									'label' => 'Link',
									'name' => 'link_page',
									'type' => 'page_link',
									'post_type' => 'page',
									'wrapper' => array(
										'width' => '50',
									),
									'conditional_logic' => array(
										array(
											array(
												'field' => 'field_sweifhdk2',
												'operator' => '==',
												'value' => 1,
											),
											array(
												'field' => 'field_121anafheha',
												'operator' => '==',
												'value' => 'page',
											),
										),
									),
								),
								array(
									'key' => 'field_7213afdg21ba',
									'label' => 'Link',
									'name' => 'link_url',
									'type' => 'url',
									'wrapper' => array(
										'width' => '50',
									),
									'conditional_logic' => array(
										array(
											array(
												'field' => 'field_sweifhdk2',
												'operator' => '==',
												'value' => 1,
											),
											array(
												'field' => 'field_121anafheha',
												'operator' => '==',
												'value' => 'url',
											),
										),
									),
								),
								array(
									'key' => 'field_zzzz721ba',
									'label' => 'Link',
									'name' => 'link_email',
									'type' => 'email',
									'wrapper' => array(
										'width' => '50',
									),
									'conditional_logic' => array(
										array(
											array(
												'field' => 'field_sweifhdk2',
												'operator' => '==',
												'value' => 1,
											),
											array(
												'field' => 'field_121anafheha',
												'operator' => '==',
												'value' => 'email',
											),
										),
									),
								),
							),
						),
						array(
							'key' => 'field_nm1h128ad',
							'label' => 'Content/Image Module',
							'name' => 'content_image_module',
							'sub_fields' => array(
								array(
									'key' => 'field_nm2hagewf',
									'name' => 'image',
									'label' => 'Image',
									'type' => 'image',
									'return_format' => 'url',
								),
								array(
									'key' => 'field_nmh1gwgdfa',
									'name' => 'content',
									'label' => 'Content',
									'type' => 'wysiwyg',
								),
								array(
									'key' => 'field_nm12afdfh12ghaf',
									'name' => 'vertical',
									'label' => 'Image Vertical?',
									'type' => 'true_false',
									'instructions' => 'Ticking this box will remove the parallax feature on desktop, give the image padding on it\'s sides and cause the image to overflow out of it\'s container on desktop.',
								),
								array(
									'key' => 'field_nm12h12ghaf',
									'name' => 'invert',
									'label' => 'Invert?',
									'type' => 'true_false',
									'instructions' => 'Ticking this box will flip the content and image containers above the desktop breakpoint.',
								),
								array(
									'key' => 'field_89dafa123',
									'label' => 'Caption?',
									'name' => 'caption',
									'type' => 'true_false',
									'instructions' => 'Ticking this box will toggle the image caption',
								),
								array(
									'key' => 'field_980zcvijaf12',
									'label' => 'Caption Text',
									'name' => 'caption_text',
									'type' => 'text',
									'instructions' => 'Keep it short and sweet! Eg: King Crab',
									'conditional_logic' => array(
										array(
											array(
												'field' => 'field_89dafa123',
												'operator' => '==',
												'value' => 1,
											),
										)
									),
								),
								array(
									'key' => 'field_mh212gasdfd',
									'label' => 'Buttons?',
									'name' => 'buttons',
									'type' => 'true_false',
									'instructions' => 'Ticking this box will enable/disable showing the buttons created below',
								),
								array(
									'key' => 'field_nm21hghaf',
									'label' => 'Button Maker',
									'name' => 'button_repeater',
									'type' => 'repeater',
									'button_label' => 'Add a Button',
									'layout' => 'block',
									'sub_fields' => array(
										array(
											'key' => 'field_n12g1gasdf',
											'label' => 'Text',
											'name' => 'text',
											'type' => 'text',
											'wrapper' => array(
												'width' => '50',
											),
										),
										array(
											'key' => 'field_n2h1gadaaewf',
											'label' => 'Style',
											'name' => 'style',
											'type' => 'select',
											'choices' => array(
												'dark' => 'Dark Green',
												'light' => 'Light Green',
											),
											'wrapper' => array(
												'width' => '50',
											),
										),
										array(
											'key' => 'field_nmahu2112',
											'label' => 'Link Type',
											'name' => 'link_type',
											'type' => 'select',
											'choices' => array(
												'file' => 'File',
												'url' => 'URL',
											),
											'wrapper' => array(
												'width' => '50',
											),
										),
										array(
											'key' => 'field_nh11gafefd',
											'label' => 'Link',
											'name' => 'link_file',
											'type' => 'file',
											'return_format' => 'url',
											'conditional_logic' => array(
												array(
													array(
														'field' => 'field_nmahu2112',
														'operator' => '==',
														'value' => 'file',
													),
												)
											),
											'wrapper' => array(
												'width' => '50',
											),
										),
										array(
											'key' => 'field_n1h1gfaef',
											'label' => 'Link',
											'name' => 'link_url',
											'type' => 'url',
											'conditional_logic' => array(
												array(
													array(
														'field' => 'field_nmahu2112',
														'operator' => '==',
														'value' => 'url',
													),
												)
											),
											'wrapper' => array(
												'width' => '50',
											),
										),
									),
									'conditional_logic' => array(
										array(
											array(
												'field' => 'field_mh212gasdfd',
												'operator' => '==',
												'value' => 1,
											),
										)
									),
								),
							),
						),
					),
				),
			),
			'location' => array (
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'page',
					),
				),
			),
		));

		acf_add_local_field_group(array(
			'key' => 'group_2',
			'title' => ' ',
			'fields' => array (
				array(
					'key' => 'field_bfgfffuawef',
					'label' => 'Hero Logo',
					'name' => 'hero-logo',
					'type' => 'image',
					'return_format' => 'url',
				),
				array(
					'key' => 'field_bfgfffubbbdfawef',
					'label' => 'Footer Logo',
					'name' => 'footer-logo',
					'type' => 'image',
					'return_format' => 'url',
				),
				array(
					'key' => 'field_234nvnpaoeae36',
					'label' => 'Google Maps API Key',
					'type' => 'text',
					'name' => 'gmaps-api-key',
					'instructions' => 'Anything here but a Google API Key won\'t work. Please make sure that on your project on console.developers.google.com has the Google Maps Geocode API and the Google Maps Javascript API enabled and it\'s restrictions are set appropriately.',
				),
				array(
					'key' => 'field_m832bad',
					'label' => 'Physical Address',
					'name' => 'address',
					'type' => 'textarea',
					'new_lines' => 'br',
				),
				array(
					'key' => 'field_m832fffbad',
					'label' => 'Hours',
					'name' => 'hours',
					'type' => 'textarea',
					'new_lines' => 'br',
				),
				array(
					'key' => 'field_87zxcv9zh12',
					'label' => 'Reservations',
					'name' => 'reservations',
					'type' => 'textarea',
					'new_lines' => 'br',
				),
				array(
					'key' => 'field_mmp3h12',
					'label' => 'Facebook URL',
					'name' => 'facebook',
					'type' => 'url',
				),
				array(
					'key' => 'field_mmp3adfh12',
					'label' => 'Twitter URL',
					'name' => 'twitter',
					'type' => 'url',
				),
				array(
					'key' => 'field_mm2323p3h12',
					'label' => 'Instagram URL',
					'name' => 'instagram',
					'type' => 'url',
				),
			),
			'location' => array (
				array (
					array (
						'param' => 'options_page',
						'operator' => '==',
						'value' => 'general-settings',
					),
				),
			),
		));
	}
}

ACFFields::_init();

?>