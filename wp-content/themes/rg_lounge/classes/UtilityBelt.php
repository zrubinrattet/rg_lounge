<?php 
	class UtilityBelt{
		public static function render_arrow($arrow_class = 'arrow', $path_class = 'arrow-path'){
			?>
				<svg class="<?php echo $arrow_class ?>" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 96.6 100">
					<path class="<?php echo $path_class ?>" d="M48,100,0,51.2l4.9-4.7,40,41.7V0h6.9V88.2L91.7,46.5l4.9,4.7L48.7,100Z"></path>
				</svg>
			<?php
		}
	}
?>