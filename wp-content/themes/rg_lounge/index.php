<?php 

get_header();

$image_module_count = 0;

if( have_rows('page-builder') ):
	while( have_rows('page-builder') ): the_row();
		
		switch ( get_row_layout() ) :

			case 'image_module':
				include(locate_template( 'modules/image.php' ));
				break;
			
			case 'content_image_module':
				include(locate_template( 'modules/image_content.php' ));
				break;

			case 'content_module':
				include(locate_template( 'modules/content.php' ));
				break;

			case 'map_module':
				include(locate_template( 'modules/map.php' ));
				break;

		endswitch;

	endwhile;
endif;

get_footer();
?>