	<footer class="footer" id="footer">
		<div class="footer-info">
			<div class="footer-info-wrapper">
				<?php 
				if( !empty(get_field('footer-logo', 'option')) ): ?>
					<div class="footer-info-logocontainer">
						<img src="<?php echo get_field('footer-logo', 'option'); ?>" class="footer-info-logocontainer-image">
					</div>
				<?php 
				endif;
				if( !empty(get_field('address', 'option')) && !empty(get_field('reservations', 'option')) && !empty(get_field('hours', 'option')) ):
				?>
				<div class="footer-info-text">
					<?php
					if( !empty(get_field('address', 'option')) && !empty(get_field('reservations', 'option')) ): ?>
						<div class="footer-info-text-left">
							<?php
							if( !empty(get_field('address', 'option')) ):?>
								<div class="footer-info-text-left-address">
									<h3 class="footer-info-text-left-address-header">location:</h3>
									<?php the_field('address', 'option'); ?>
								</div>
							<?php 
							endif; 
							if( !empty(get_field('reservations', 'option')) ): ?>
								<div class="footer-info-text-left-reservations">
									<h3 class="footer-info-text-left-reservations-header">Reservations:</h3>
									<?php the_field('reservations', 'option'); ?>
								</div>
							<?php
							endif;
							?>
						</div>
					<?php
					endif;
					if( !empty(get_field('hours', 'option')) ):?>
						<div class="footer-info-text-right">
							<div class="footer-info-text-right-hours">
								<h3 class="footer-info-text-right-hours-header">hours:</h3>
								<?php the_field('hours', 'option'); ?>
							</div>
						</div>
					<?php 
					endif;
					?>
				</div>
				<?php 
				endif;
				?>
			</div>
		</div>
		<?php if( empty(get_field('facebook', 'option')) && empty(get_field('twitter', 'option')) && empty(get_field('instagram', 'option')) ): ?>

		<?php else: ?>
			<div class="footer-social">
				<h2 class="footer-social-header">Find us online:</h2>
				<div class="footer-social-icons">
					<?php if(!empty(get_field('facebook', 'option'))): ?>
						<a href="<?php echo get_field('facebook', 'option'); ?>" class="footer-social-icons-link">
							<i class="footer-social-icons-link-icon fa fa-facebook"></i>
						</a>
					<?php endif; ?>
					<?php if(!empty(get_field('twitter', 'option'))): ?>
						<a href="<?php echo get_field('twitter', 'option'); ?>" class="footer-social-icons-link">
							<i class="footer-social-icons-link-icon fa fa-twitter"></i>
						</a>
					<?php endif; ?>
					<?php if(!empty(get_field('instagram', 'option'))): ?>
						<a href="<?php echo get_field('instagram', 'option'); ?>" class="footer-social-icons-link">
							<i class="footer-social-icons-link-icon fa fa-instagram"></i>
						</a>
					<?php endif; ?>
				</div>
			</div>
		<?php endif; ?>
	</footer>
<?php
	if (in_array($_SERVER['REMOTE_ADDR'], array('127.0.0.1', '::1'))) {
	   ?> <script src="//localhost:35729/livereload.js"></script> <?php
	}
	?>
</body>
</html>