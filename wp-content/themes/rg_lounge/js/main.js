;(function ( $, window, document, undefined ) {

	$(document).ready(function(){

        // this is a global component used to determine which breakpoint we're at
        // to listen for the event do $(document).on('breakpoint', eventHandlerCallback);
        // callback accepts normal amount of params that a js event listener callback would have
        // but there's a custom property added to the event object called "name"
        // assuming in the callback the event object variable passed in is e: console.log(e.device);
        Breakpoint = {
            name : '',
            _init : function(){
                $(window).on('resize load', Breakpoint._resizeLoadHander);
            },
            _resizeLoadHander : function(){
                if( $(window).width() > 1024 && Breakpoint.name != 'desktop' ){
                    Breakpoint.name = 'desktop';
                    Breakpoint._dispatchEvent();
                }
                else if( $(window).width() <= 1024 && $(window).width() > 640 && Breakpoint.name != 'tablet' ){
                    Breakpoint.name = 'tablet'; 
                    Breakpoint._dispatchEvent();
                }
                else if( $(window).width() < 641 && Breakpoint.name != 'mobile' ){      
                    Breakpoint.name = 'mobile';     
                    Breakpoint._dispatchEvent();
                }
            },
            _dispatchEvent : function(){
                $(document).trigger($.Event('breakpoint', {device: Breakpoint.name}));
            }
        }
        Breakpoint._init(); 

		var MobileNav = {
			toggle : $('.nav-mobile'),
			hamburger : $('.nav-mobile-hamburger'),
			nav : $('.nav'),
			menu : $('.nav-menu'),
			hero : $('.hero'),
            arrow : $('.hero-arrow'),
			mobile : null,
            reservations : $('.nav-menu-item-link[href="#footer"]'),
			_init : function(){
				MobileNav._mobileCheck();
				MobileNav.toggle.on('click', MobileNav._clickHandler);
				$(window).on('resize load scroll', MobileNav._resizeLoadScrollHandler);
                MobileNav.arrow.on('click', MobileNav._arrowClickHandler);
                MobileNav.reservations.on('click', MobileNav._reservationsClickHandler);
			},
			_mobileCheck : function(){
				if($(window).width() > 1024){
					MobileNav.mobile = false;
				}
				else{
					MobileNav.mobile = true;
				}
			},
            _arrowClickHandler : function(){
                $('html, body').animate({
                    scrollTop : MobileNav.hero.height(),
                }); 
            },
			_clickHandler : function(){
				if(MobileNav.nav.hasClass('nav--visible') == false){
					MobileNav._open();
				}
				else{
					MobileNav._close();	
				}
				if($(window).scrollTop() < MobileNav.hero.height() - MobileNav.toggle[0].clientHeight){
					$('html, body').animate({
						scrollTop : MobileNav.hero.height(),
					});
				}
			},
            _reservationsClickHandler : function(e){
                e.preventDefault();
                if( $(window).width() < 1025 ){
                    MobileNav._close();
                }
                $('html, body').animate({
                    scrollTop : $('#footer').offset().top - ($(window).width() > 1024 ? 0 : MobileNav.toggle[0].clientHeight)
                });
            },
			_open : function(){
				MobileNav.nav.addClass('nav--visible');
				MobileNav.hamburger.removeClass('fa-bars');
				MobileNav.hamburger.addClass('fa-times');
				MobileNav.menu.slideDown();
			},
			_close : function(){
				MobileNav.nav.removeClass('nav--visible');
				MobileNav.hamburger.removeClass('fa-times');
				MobileNav.hamburger.addClass('fa-bars');
				MobileNav.menu.slideUp();
			},
			_resizeLoadScrollHandler : function(e){
				// open nav if in desktop else close
				MobileNav._mobileCheck();
				if(e.type !== 'scroll'){
					if($(window).width() > 1024){
						if(MobileNav.mobile){
							MobileNav._open();
						}
					}	
					else{
						MobileNav._close();
					}
				}
				if($(window).scrollTop() > MobileNav.hero[0].clientHeight - ( $(window).width() > 1024 ? 112 : 112 ) ){
					MobileNav.nav.addClass('nav--sticky');
				}
				else{
					MobileNav.nav.removeClass('nav--sticky');	
				}
			},
		};
		MobileNav._init();

		var Parallax = {
			strength : 25,
			stronger : 40,
			imagesections : $('.imagemodule-image'),
			imagecontentsections : $('.imagecontentmodule:not(.vertical) .imagecontentmodule-imagecontainer'),
			_init : function(){
				$(window).on('resize scroll load', Parallax._resizeLoadScrollHandler);
			},
			_map : function(n,i,o,r,t){return i>o?i>n?(n-i)*(t-r)/(o-i)+r:r:o>i?o>n?(n-i)*(t-r)/(o-i)+r:t:void 0;},
			_getAmount : function(el, strength){
				var windowcenter = $(window).scrollTop() + ( $(window).height() / 2 );
				var sectioncenter = $(el).offset().top + ($(el).height() / 2);
				var sectiondelta = windowcenter - sectioncenter;
				var scale = Parallax._map(sectiondelta, 0, $(window).height(), 0, strength);
				return -50 + scale + '%';
			},
			_resizeLoadScrollHandler : function(){
				for(var i = 0; i < Parallax.imagesections.length; i++){
					$(Parallax.imagesections[i]).css('transform', 'translate3d(-50%, ' + Parallax._getAmount(Parallax.imagesections[i], Parallax.strength) + ',0)');
				}
				if($(window).width() > 1024){
					for(var i = 0; i < Parallax.imagecontentsections.length; i++){
						$(Parallax.imagecontentsections[i].children[0]).css('transform', 'translate3d(-50%, ' + Parallax._getAmount(Parallax.imagecontentsections[i], Parallax.stronger) + ',0)');
					}
				}
				else{
					for(var i = 0; i < Parallax.imagecontentsections.length; i++){
						$(Parallax.imagecontentsections[i].children[0]).removeAttr('style');
					}
				}
			}
		}
		Parallax._init();

        var ContentImageCaptionHandler = {
            captions : $('.imagecontentmodule-imagecontainer-caption'),
            _init : function(){
                if( !$('body').hasClass('home') ){
                    $(document).on('breakpoint', ContentImageCaptionHandler._breakpointHandler);
                    $(window).on('resize', ContentImageCaptionHandler._resizeHandler);
                }
            },
            _resizeHandler : function(){
                if( Breakpoint.name == 'desktop' ){
                    ContentImageCaptionHandler._setCSS();
                }
            },
            _breakpointHandler : function(e){
                if(e.device == 'desktop'){
                    ContentImageCaptionHandler._setCSS();
                }
                else{
                    ContentImageCaptionHandler._unsetCSS();   
                }
            },
            _setCSS : function(){
                for(var i = 0; i < ContentImageCaptionHandler.captions.length; i++){
                    var imageHeight = $(ContentImageCaptionHandler.captions[i]).parent().find('.imagecontentmodule-imagecontainer-image').height();
                    var containerHeight = $(ContentImageCaptionHandler.captions[i]).parent().height();
                    var invertDelta = (imageHeight - containerHeight) * -1;

                    var module = $(ContentImageCaptionHandler.captions[i]).parent().parent();

                    var prev = module.prev();
                    if( prev.hasClass('imagemodule') && !module.hasClass('invert') ){
                        prev.find('.imagemodule-caption').css({
                            right: '0%',
                            left: 'inherit',
                        });
                    }

                    $(ContentImageCaptionHandler.captions[i]).css('bottom', invertDelta / 2);
                }
            },
            _unsetCSS : function(){
                ContentImageCaptionHandler.captions.removeAttr('style');
                $('.imagemodule-caption').removeAttr('style');
            },
        };

        ContentImageCaptionHandler._init();

	});

})( jQuery, window, document );

(function($) {

/*
*  new_map
*
*  This function will render a Google Map onto the selected jQuery element
*
*  @type	function
*  @date	8/11/2013
*  @since	4.3.0
*
*  @param	$el (jQuery element)
*  @return	n/a
*/

function new_map( $el ) {
	
	// var
	var $markers = $el.find('.mapmodule-marker');
	
	
	// vars
	var args = {
		zoom		: 16,
		center		: new google.maps.LatLng(0, 0),
		mapTypeId	: google.maps.MapTypeId.ROADMAP,
		disableDefaultUI: true,
		styles      : [
    {
        "featureType": "administrative",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#444444"
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "all",
        "stylers": [
            {
                "color": "#f2f2f2"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "poi.business",
        "elementType": "geometry",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "poi.business",
        "elementType": "labels",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "poi.business",
        "elementType": "labels.text",
        "stylers": [
            {
                "visibility": "on"
            }
        ]
    },
    {
        "featureType": "poi.business",
        "elementType": "labels.icon",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "poi.park",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "lightness": "3"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "all",
        "stylers": [
            {
                "saturation": -100
            },
            {
                "lightness": 45
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "geometry",
        "stylers": [
            {
                "hue": "#ff0000"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "labels",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "hue": "#ff0000"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "labels.text",
        "stylers": [
            {
                "hue": "#ff7900"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#236030"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "labels.text.stroke",
        "stylers": [
            {
                "hue": "#ff00e1"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "labels.icon",
        "stylers": [
            {
                "saturation": "0"
            },
            {
                "hue": "#ff0000"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "simplified"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "all",
        "stylers": [
            {
                "hue": "#ff0000"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "labels.icon",
        "stylers": [
            {
                "visibility": "off"
            },
            {
                "hue": "#ff0000"
            }
        ]
    },
    {
        "featureType": "transit",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "all",
        "stylers": [
            {
                "color": "#94d7f2"
            },
            {
                "visibility": "on"
            }
        ]
    }
]


,
 	};
	
	
	// create map	        	
	var map = new google.maps.Map( $el[0], args);
	map.setOptions({draggable: false, zoomControl: false, scrollwheel: false, disableDoubleClickZoom: true});

	
	// add a markers reference
	map.markers = [];
	
	
	// add markers
	$markers.each(function(){
		
    	add_marker( $(this), map );
		
	});
	
	
	// center map
	center_map( map );
	
	
	// return
	return map;
	
}

/*
*  add_marker
*
*  This function will add a marker to the selected Google Map
*
*  @type	function
*  @date	8/11/2013
*  @since	4.3.0
*
*  @param	$marker (jQuery element)
*  @param	map (Google Map object)
*  @return	n/a
*/

function add_marker( $marker, map ) {

	// var
	var latlng = new google.maps.LatLng( $marker.attr('data-lat'), $marker.attr('data-lng') );

	// create marker
	var marker = new google.maps.Marker({
		position	: latlng,
		map			: map
	});

	// add to array
	map.markers.push( marker );

	// if marker contains HTML, add it to an infoWindow
	if( $marker.html() )
	{
		// create info window
		var infowindow = new google.maps.InfoWindow({
			content		: $marker.html()
		});

		// show info window when marker is clicked
		google.maps.event.addListener(marker, 'click', function() {

			infowindow.open( map, marker );

		});
	}

}

/*
*  center_map
*
*  This function will center the map, showing all markers attached to this map
*
*  @type	function
*  @date	8/11/2013
*  @since	4.3.0
*
*  @param	map (Google Map object)
*  @return	n/a
*/

function center_map( map ) {

	// vars
	var bounds = new google.maps.LatLngBounds();

	// loop through all markers and create bounds
	$.each( map.markers, function( i, marker ){

		var latlng = new google.maps.LatLng( marker.position.lat(), marker.position.lng() );

		bounds.extend( latlng );

	});

	// only 1 marker?
	if( map.markers.length == 1 )
	{
		// set center of map
	    map.setCenter( bounds.getCenter() );
	    map.setZoom( 16 );
	}
	else
	{
		// fit to bounds
		map.fitBounds( bounds );
	}

}

/*
*  document ready
*
*  This function will render each map when the document is ready (page has loaded)
*
*  @type	function
*  @date	8/11/2013
*  @since	5.0.0
*
*  @param	n/a
*  @return	n/a
*/
// global var
var map = null;

$(document).ready(function(){

	$('.mapmodule').each(function(){

		// create map
		map = new_map( $(this) );

	});

});

})(jQuery);