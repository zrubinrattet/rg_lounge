<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<title><?php echo get_bloginfo('name') . ' | ' . (!is_null($post->post_name) ? ucwords($post->post_name) : '404')?></title>
		<?php wp_head(); ?>		

		<!--[if lt IE 9]>
		<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
	</head>
	<body <?php body_class(); ?>>
	<?php 
		include(locate_template( 'modules/hero.php' ));
		include(locate_template( 'partials/nav.php' ));
	?>