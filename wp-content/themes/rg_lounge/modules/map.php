<?php 

$location = get_sub_field('map');

if( !empty($location) ):
?>
<div class="mapmodule">
	<div class="mapmodule-marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
</div>
<?php endif; ?>