<section class="contentmodule"<?php echo !empty(get_sub_field('bg_color')) ? ' style="background-color: ' . get_sub_field('bg_color') . ';"' : ''; ?>>
	<?php if( !empty(get_sub_field('content')) ): ?>
		<div class="contentmodule-content">
			<?php the_sub_field('content'); ?>
		</div>
	<?php endif; ?>
</section>