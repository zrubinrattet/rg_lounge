<?php global $post; ?>
<section class="hero"<?php echo !empty(get_field('hero-image', $post->ID)) ? ' style="background-image: url(\'' . get_field('hero-image', $post->ID) . '\'"' : ''; ?>>
	<?php if( !empty(get_field('hero-logo', 'option')) ): ?>
		<a href="<?php echo site_url(); ?>"><img src="<?php echo get_field('hero-logo', 'option'); ?>" class="hero-logo"></a>
	<?php endif; ?>
	<?php UtilityBelt::render_arrow('hero-arrow', 'hero-arrow-path'); ?>
</section>