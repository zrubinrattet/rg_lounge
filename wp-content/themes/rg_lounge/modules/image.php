<?php $image_module_count++; ?>
<section class="imagemodule<?php echo $image_module_count & 1 ? ' green' : ' red'; echo is_home() ? '' : ' nothome'; ?>">
	<?php 
	if( get_sub_field('caption_toggle') ):
		if( !empty(get_sub_field('caption')) ):
			if( get_sub_field('link_toggle') ):
				$link = '';
				$type = get_sub_field('link_type');
				if( $type == 'page' || $type == 'url' ):
					$link = get_sub_field('link_' . $type);
				else:
					$link = 'mailto:' . get_sub_field('link_email');
				endif;
	?>
			<a <?php echo get_sub_field('link_newtab') ? 'target="_blank"' : ''; ?> href="<?php echo !empty($link) ? $link : '#' ?>" class="imagemodule-caption"><?php echo !empty(get_sub_field('caption')) ? '<span>' . get_sub_field('caption') . '</span>' . UtilityBelt::render_arrow('imagemodule-caption-arrow', 'imagemodule-caption-arrow-path') : 'Please add caption'; ?></a>
	<?php 
			else:
	?>
			<div class="imagemodule-caption nolink"><?php echo !empty(get_sub_field('caption')) ? get_sub_field('caption') . UtilityBelt::render_arrow('imagemodule-caption-arrow', 'imagemodule-caption-arrow-path') : 'Please add caption'; ?></div>
	<?php
			endif;
		endif;
	endif; ?>
	<img class="imagemodule-image" src="<?php the_sub_field('image'); ?>">
</section>