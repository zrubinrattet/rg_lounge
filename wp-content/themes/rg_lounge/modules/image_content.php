<section class="imagecontentmodule<?php echo get_sub_field('invert') ? ' invert' : ''; ?><?php echo get_sub_field('vertical') ? ' vertical' : ''; ?>">
	<div class="imagecontentmodule-imagecontainer">
		<?php if( get_sub_field('caption') ): ?>
			<div class="imagecontentmodule-imagecontainer-caption">
				<?php the_sub_field('caption_text'); ?>
			</div>
		<?php endif; ?>
		<img src="<?php the_sub_field('image'); ?>" class="imagecontentmodule-imagecontainer-image">	
	</div>
	<div class="imagecontentmodule-contentcontainer">
		<div class="imagecontentmodule-contentcontainer-content">
			<?php the_sub_field('content'); ?>
		</div>
		<?php if( get_sub_field('buttons') && have_rows('button_repeater') ): ?>
			<div class="imagecontentmodule-contentcontainer-buttons">
				<?php while( have_rows('button_repeater') ): the_row(); ?>
					<a href="<?php echo get_sub_field('link_type') == 'file' ? get_sub_field('link_file') : get_sub_field('link_url'); ?>" class="imagecontentmodule-contentcontainer-buttons-button<?php echo ' ' . get_sub_field('style') ?>" target="_blank">
						<?php the_sub_field('text'); ?>
					</a>
				<?php endwhile; ?>
			</div>
		<?php endif; ?>
	</div>
</section>