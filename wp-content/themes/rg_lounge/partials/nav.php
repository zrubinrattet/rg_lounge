<?php 

require get_template_directory() . '/classes/NavWalker.php';

wp_nav_menu(array(
	'container' => 'nav',
	'container_class' => 'nav',
	'items_wrap' => '<div class="nav-mobile"><i class="nav-mobile-hamburger fa fa-bars"></i></div><ul class="nav-menu">%3$s</ul>',
	'walker' => new NavWalker(),
));

?>